//
//  MessageFactory.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 4/16/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class MessageFactory: NSObject {

    static func createMessage(withDict dict: [String:Any], fromSource source: Int) -> BaseMessage {
        switch source {
        case MessageSource.bot.rawValue, MessageSource.user.rawValue:
            if let msg = dict["message"] as? String {
                return Message(message: msg, source: source)
            }
            return Message(message: "", source: source)
        case MessageSource.movie.rawValue, MessageSource.emptyMovie.rawValue:
            return Movies(dict: dict)
        default:
            return Message(message: "", source: 0)
        }
    }
    
}
