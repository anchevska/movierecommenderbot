//
//  Movies.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 5/14/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class Movies: BaseMessage {

    var movies: [Movie]
    var state_type: StateType?
    
    override init() {
        movies = [Movie]()
        super.init()
    }
    
    init(dict: [String:Any]) {
        movies = [Movie]()
        if let stateType = dict["type"] as? String, let state = StateType(rawValue: stateType) {
            state_type = state
        }
        if let moviesList = dict["movies"] as? [[String: Any]] {
            for movieDict in moviesList {
                let movie = Movie(dict: movieDict)
                movie.state_type = self.state_type
                movies.append(movie)
            }
        }
        super.init()
        if let src =  dict["source"] as? Int, let value = MessageSource(rawValue: src) {
            source = value
        }
    }
    
    func getMovie(withIndex index: Int) -> Movie? {
        return movies[index]
    }
    
    func getCount() -> Int {
        return movies.count
    }
}
