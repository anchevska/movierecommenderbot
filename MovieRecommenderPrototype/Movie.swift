//
//  Movie.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 2/26/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class Movie: BaseMessage {
    
    static let imdbStaticLink = "http://www.imdb.com/title/"
    static let tmdbStaticLink = "https://www.themoviedb.org/movie/"
    static let MovieLensStaticLink = "https://movielens.org/movies/"
    
    var title: String
    var imdbID: String?
    var tmdbID: String?
    var imageLink: String?
    var movieID: Int?
    var releaseYear: Int?
    var state_type: StateType?
    
    init(dict: [String:Any]) {
        title = dict["title"] as! String
        super.init()
        if let src =  dict["source"] as? Int, let value = MessageSource(rawValue: src) {
            source = value
        }
        imdbID = dict["imdbID"] as? String
        tmdbID = dict["tmdbID"] as? String
        if let imgLink = dict["imageLink"] as? String {
            imageLink = imgLink
        }
        movieID = dict["movieID"] as? Int
        releaseYear = dict["releaseYear"] as? Int
    }
}
