//
//  Message.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 3/27/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class Message: BaseMessage {
    
    static let maxWidth = (UIScreen.main.bounds.size.width-(2*16))/3 * 2
    var size: CGSize!
    
    init(dict: [String: Any]) {
        super.init()
        if let msg = dict["message"] as? String {
            message = msg
        }
        if let src =  dict["source"] as? Int, let value = MessageSource(rawValue: src) {
            source = value
        }
        size = getSize()
    }
    
    init(message msg: String, source src: Int) {
        super.init()
        message = msg
        source = MessageSource(rawValue:src)!
        size = getSize()
    }
    
    
    func getSize() -> CGSize {
        return message.boundingRect(with: CGSize(width: Message.maxWidth, height: CGFloat.greatestFiniteMagnitude),
                                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                            attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 15)],
                                                            context: nil).size
    }

}
