//
//  MessageView.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 4/17/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class MessageView: UIView {
    
    @IBOutlet var messageTextField: UITextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet var messageViewHeightConstraint: NSLayoutConstraint!
    var link: String?
    var links : [[String : Any]] = []
    weak var delegate: ChoiceButtonsDelegate?
    
    func show() {
        messageTextField.text = ""
        UIView.animate(withDuration: 0.2) {
            self.messageViewHeightConstraint.constant = 56
            self.sendMessageButton.isHidden = false
            self.messageTextField.isHidden = false
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 1) {
            self.messageViewHeightConstraint.constant = 0
            self.sendMessageButton.isHidden = true
            self.messageTextField.isHidden = true
        }
    }
    
    @IBAction func sendClicked(_ sender: Any){
        if let text = messageTextField.text , text != "" {
            delegate?.hideButtons()
            updateLinksForScenarios()
            if let link = link {
                StateManager.sharedInstance.redirectScenario(link: link, title: text)
                messageTextField.resignFirstResponder()
            }
        }
    }
    
    func updateLinks(withArray array: [[String : Any]]) {
        links = []
        for choiceDict in array {
            links.append(choiceDict)
        }
    }
    
    func updateLink(type: String) {
        self.link = nil
        for link in links {
            if let descriptions = link["descriptions"] as? [String],
                let description = descriptions.first {
                if let linkName = link["link"] as? String, description == type {
                    self.link = linkName
                    break
                }
            }
        }
    }
    
    func updateLinksForScenarios(){
        if let type = StateManager.sharedInstance.type {
            switch type {
            case .enterGenre, .showGenres:
                let message = self.messageTextField.text!.lowercased()
                if Constants.genres.contains(message) {
                    StateManager.sharedInstance.genre = message
                    updateLink(type: "right")
                }
                else {
                    updateLink(type: "wrong")
                }
            case .enterYear:
                if let year = findYearInText() {
                    StateManager.sharedInstance.year = year
                    updateLink(type: "right")
                }
                else {
                    updateLink(type: "wrong")
                }
            default:
                break
            }
        }
    }
    
    func findYearInText() -> String? {
        let regexInterval = "(200[0-9]|201[0-6]|199[0-9])-(200[0-9]|201[0-6]|199[0-9])"
        let regexYear = "(200[0-9]|201[0-6]|199[0-9])"
        
        if let range = self.messageTextField.text!.range(of:regexInterval, options: .regularExpression) {
            return self.messageTextField.text!.substring(with:range)
        }
        if let range = self.messageTextField.text!.range(of:regexYear, options: .regularExpression) {
            return self.messageTextField.text!.substring(with:range)
        }
        return nil
    }
}
