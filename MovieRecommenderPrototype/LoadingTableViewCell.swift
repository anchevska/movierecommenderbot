//
//  LoadingTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 5/7/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingCell: MessageTableViewCell {
    
    @IBOutlet var loadingIndicatorView: UIView!
    
    override func setupCell(withMessage message: BaseMessage){
        for view in self.view.subviews{
            view.removeFromSuperview()
        }
        
        let message = message as! Message
        super.setupCell(withMessage: message)
        let padding : CGFloat = 24
        let roundedView = UIView(frame: CGRect(x:16,y:0,width:message.size.width + padding,height:message.size.height + padding))
        let maskPath = UIBezierPath(roundedRect: roundedView.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight, .topLeft],
                                    cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        view.layer.mask = shape
        
        labelWidthConstraint.constant = message.size.width + 48
        layoutIfNeeded()
        
        addAnimation()
    }
    
    func addAnimation(){
        let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 2, y: 10, width: 40, height: 20), type: NVActivityIndicatorType.ballPulseSync, color: UIColor(colorLiteralRed: 96/255, green: 105/255, blue: 119/255, alpha: 1.0))
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        super.layoutSubviews()
    }
    
}

