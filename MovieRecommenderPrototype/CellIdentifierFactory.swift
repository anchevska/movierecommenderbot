//
//  CellIdentifierFactory.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 4/1/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class CellIdentifierFactory: NSObject {

    static func getCellIdentifier(withMessageSource source: MessageSource) -> String {
        switch source {
        case .bot:
            return "botCell"
        case .user:
            return "userCell"
        case .movie:
            return "movieCell"
        case .loading:
            return "loadingCell"
        case .emptyMovie:
            return "emptyMovieCell"
        }
    }
}
