//
//  MovieTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 5/14/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class MovieTableViewCell: BaseTableViewCell {
    
    @IBOutlet fileprivate weak var collectionView: UICollectionView!

}

extension MovieTableViewCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
}
