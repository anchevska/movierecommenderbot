//
//  EmptyMovieTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 5/17/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class EmptyMovieTableViewCell: BaseTableViewCell {
    
    override func setupCell(withMessage message: BaseMessage){
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
