//
//  MessageTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 3/27/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class MessageTableViewCell: BaseTableViewCell {
    
    @IBOutlet var labelWidthConstraint: NSLayoutConstraint!
    @IBOutlet var view : UIView!
    
    let cornerRadius = 10.0


    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupCell(withMessage message: BaseMessage) {
        super.setupCell(withMessage: message)
    }
}
