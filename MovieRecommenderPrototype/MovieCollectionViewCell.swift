//
//  MovieCollectionViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 5/14/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyStarRatingView

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var movieImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var ratingView: SwiftyStarRatingView!
    @IBOutlet var actionButton: UIButton!
    
    var movie: Movie!
    
    func setupCell(withMessage message: BaseMessage) {
        self.movie = message as! Movie
        self.movieImage.image = nil
        if let imgLink = movie.imageLink {
            Alamofire.request(imgLink).responseImage { response in
                if let img = response.result.value {
                    self.movieImage.image = img
                }
            }
        }
        titleLabel.text = movie.title
        if let year = movie.releaseYear {
            yearLabel.text = String(year)
        }
        setupRatingView()
        setupActionButton()
    }
    
    func setupActionButton() {
        if let type = movie.state_type, type == .showRecommendations || type == .showQuestionsRecommendations {
            actionButton.setTitle("Add to wishlist", for: .normal)
        }
        else {
            actionButton.setTitle("Skip this movie", for: .normal)
        }
    }
    
    @IBAction func actionButtonClicked(_ sender: Any) {
        if let type = movie.state_type, type == .showRecommendations || type == .showQuestionsRecommendations {
            MovieManager.sharedInstance.addToWishlist(movie: movie)
        }
        else {
            MovieManager.sharedInstance.notInterested(movie: movie)
        }
    }
    
    func setupRatingView(){
        ratingView.value = 0
        ratingView.tintColor = UIColor(colorLiteralRed: 88/255, green: 64/255, blue: 173/255, alpha: 1.0)
        ratingView.accurateHalfStars = false
        ratingView.addTarget(self, action: #selector(ratingAdded), for: .valueChanged)
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 1
    }
    
    func ratingAdded(){
        if ratingView.value != 0 {
            MovieManager.sharedInstance.ratingAdded(movie: movie, rating: ratingView.value)
        }
    }
    
}
