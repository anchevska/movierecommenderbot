//
//  ChoiceButton.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 4/1/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import Alamofire

protocol ChoiceButtonsDelegate: class {
    func refreshAllButtons(withArray array: [[String:Any]])
    func hideButtons()
    func setupKeyboard(withArray array: [[String:Any]])
}

class ChoiceButton: UIButton {
    
    var title: String!
    var link: String!
    @IBOutlet var buttonWidthConstraint: NSLayoutConstraint!
    weak var delegate: ChoiceButtonsDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setup(fromDict dict: [String:Any]){
        if let array = dict["descriptions"] as? [String] {
            let randomIndex = Int(arc4random_uniform(UInt32(array.count)))
            title = array[randomIndex]
            setTitle(title, for: .normal)
            let width = getSize(message: title)
            buttonWidthConstraint.constant = width + 32
            isHidden = false
        }
        else {
            title = ""
        }
        if let scenarioLink = dict["link"] as? String {
            link = scenarioLink
        }
        else {
            link = ""
        }
    }
    
    func reset(){
        isHidden = true
        buttonWidthConstraint.constant = 0
        setTitle("", for: .normal)
    }
    
    func buttonClicked() {
        delegate?.hideButtons()
        StateManager.sharedInstance.redirectScenario(link: link, title: title)
    }
    
    func getSize(message: String) -> CGFloat {
        return message.boundingRect(with: CGSize(width: Message.maxWidth, height: CGFloat.greatestFiniteMagnitude),
                                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                    attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 15)],
                                    context: nil).size.width
    }

    
}
