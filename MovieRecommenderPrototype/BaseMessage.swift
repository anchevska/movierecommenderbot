//
//  BaseMessage.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 4/16/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class BaseMessage: NSObject {

    var source: MessageSource
    var message: String
    
    override init(){
        message = ""
        source = MessageSource(rawValue:0)!
        super.init()
    }

    
}
