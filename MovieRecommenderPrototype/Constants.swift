//
//  Constants.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 2/25/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import Foundation

enum StateType: String {
    case showMovies = "showMovies"
    case showWishlist = "showWishlist"
    case showRecommendations = "showRecommendations"
    case showQuestionsRecommendations = "showQuestionsRecommendations"
    case enterGenre = "enterGenre"
    case showGenres = "showGenres"
    case enterYear = "enterYear"
    case happyUser = "happyUser"
    case sadUser = "sadUser"
}

enum OptionConfiguration: Int {
    case none = 0
    case keyboard = 1
    case buttons = 2
}

enum MessageSource: Int {
    case bot = 0
    case user = 1
    case movie = 2
    case loading = 3
    case emptyMovie = 4
}

enum RefreshType: Int {
    case reload = 2
    case insert = 3
}

class Constants {
    
    static let tmdbAPIKey = "6541e072358b81eb11fab7a67a979491"
    static let delay = 1.5
    static let userOpensAppMessages = ["Hey","Hello", "Yo", "Hi", "Hey hey"]
    static let genres = ["comedy","drama","romance","action","crime","western","fiction","mystery","thriller","adventure","music","fantasy","animation","family","history","war","foreign","documentary","horror","tv movie"]
    
}

