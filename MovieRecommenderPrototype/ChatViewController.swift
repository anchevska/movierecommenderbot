//
//  ChatViewController.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 3/27/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import Alamofire
import XLActionController
import NVActivityIndicatorView

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ChoiceButtonsDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var choicesButtons: [ChoiceButton]!
    @IBOutlet var choicesViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var choicesViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var choicesView: UIView!
    @IBOutlet var messageView: MessageView!
    @IBOutlet var tableView : UITableView!
    var activityIndicator : NVActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.setValue(nil, forKey: "userEmotion")
        setDelegateToChoiceButtons()
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 64, right: 0)
        if !MessageManager.sharedInstance.messages.isEmpty {
            tableView.scrollToRow(at: IndexPath(row:MessageManager.sharedInstance.messages.count-1 , section:0), at: .bottom, animated: true)
        }
        listenForKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(userOpenedApp), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
        shadowView.layer.shadowRadius = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideButtons()
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50), type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor(colorLiteralRed: 88/255, green: 64/255, blue: 173/255, alpha: 1.0))
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func userOpenedApp() {
        getChoices()
    }
    
    func getChoices() {
        hideButtons()
        StateManager.sharedInstance.getChoices { (choicesArray) in
            var arr = choicesArray
            if choicesArray.isEmpty {
                let index = arc4random_uniform(2) + 1
                let choice : [String : Any] = ["descriptions": Constants.userOpensAppMessages, "link": "user-opens-app-\(index)"]
                arr = [choice]
                StateManager.sharedInstance.configuration = .buttons
            }
            if let firstChoice = choicesArray.first, let link = firstChoice["link"] as? String, link == "onboarding-start" {
                StateManager.sharedInstance.redirectScenario(link: link, title: "")
            }
            else {
                self.setupOptions(choices: arr)
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTable), name:NSNotification.Name(rawValue:"modelRefreshed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showOptions), name: NSNotification.Name(rawValue: "messageSent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(initialLoadingFinished), name: NSNotification.Name(rawValue: "initialLoadingFinished"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Keyboard
    
    func listenForKeyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardIsShown), name: NSNotification.Name.UIKeyboardDidShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardIsHidden), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
    }
    
    func keyboardIsShown(_ notification: Notification){
        let keyboardSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        UIView.animate(withDuration: 0.2, animations: {()->Void in
            self.tableViewBottomConstraint.constant = keyboardSize.height
            self.view.layoutIfNeeded()
            self.view.updateConstraints()
            self.scrollTable()
        })
    
    }
    
    func keyboardIsHidden(_ notification: Notification){
        UIView.animate(withDuration: 0.5, animations: {()->Void in
            self.tableViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
            self.view.updateConstraints()
        })
    }
    
    //MARK: STATE
    
    func setDelegateToChoiceButtons(){
        for button in choicesButtons {
            button.delegate = self
        }
        messageView.delegate = self
    }
    
    func hideButtons() {
        UIView.animate(withDuration: 1) {
            for button in self.choicesButtons {
                button.reset()
            }
            self.choicesViewHeightConstraint.constant = 0
        }
        messageView.hide()
    }
    
    func refreshAllButtons(withArray array: [[String : Any]]) {
        var choicesViewWidth : CGFloat = 0
        for (index,choiceDict) in array.enumerated() {
            self.choicesButtons[index].setup(fromDict: choiceDict)
            choicesViewWidth += self.choicesButtons[index].buttonWidthConstraint.constant + 16
        }
        UIView.animate(withDuration: 1) {
            self.choicesViewHeightConstraint.constant = 56
        }
        self.choicesViewWidthConstraint.constant = choicesViewWidth
    }
    
    func setupKeyboard(withArray array: [[String : Any]]) {
        messageView.updateLinks(withArray: array)
        showKeyboard()
    }
    
    func showKeyboard() {
        messageView.show()
    }
    
    //MARK: Table view delegate methods
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let message = MessageManager.sharedInstance.getMessage(withIndex: indexPath.row){
            let identifier = CellIdentifierFactory.getCellIdentifier(withMessageSource: message.source)
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! BaseTableViewCell
            if identifier == "movieCell"{
                (cell as! MovieTableViewCell).setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            }
            else if identifier == "emptyMovieCell"{
                cell.setupCell(withMessage: message)
            }
            else {
                cell.setupCell(withMessage: message as! Message)
            }
            cell.isUserInteractionEnabled = indexPath.row == MessageManager.sharedInstance.messages.count - 1
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageManager.sharedInstance.messages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = MessageManager.sharedInstance.getMessage(withIndex: indexPath.row)
        if message is Movies {
            return 180
        }
        else {
            if let message = MessageManager.sharedInstance.getMessage(withIndex: indexPath.row) as? Message {
                if message.message == "" {
                    return 0
                }
                return message.size.height + 32
            }
        }
        return 0
    }
    
    func refreshTable(){
        reloadTable()
        if MessageManager.sharedInstance.messages.count > 0 {
            tableView.scrollToRow(at: IndexPath(row:MessageManager.sharedInstance.messages.count-1 , section:0), at: .bottom, animated: true)
        }
    }
    
    func reloadTable(){
        let count = MessageManager.sharedInstance.messages.count
        if count > 1 {
            tableView.beginUpdates()
            switch MessageManager.sharedInstance.refreshType {
            case .reload:
                tableView.reloadRows(at: [IndexPath(row: count - 1, section: 0)], with: .top)
            case .insert:
                tableView.insertRows(at: [IndexPath(row: count - 1, section: 0)], with: .top)
            }
            tableView.endUpdates()
            scrollTable()
        }
        else {
            tableView.reloadData()
        }
    }
    
    func scrollTable(){
        tableView.scrollToRow(at: IndexPath(row:MessageManager.sharedInstance.messages.count-1 , section:0), at: .bottom, animated: true)
    }
    
    func initialLoadingFinished() {
        getChoices()
        activityIndicator.stopAnimating()
    }
    
    //MARK: UICollectionView delegate method 
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let moviesModel = MessageManager.sharedInstance.getMessage(withIndex: collectionView.tag) as? Movies {
            return moviesModel.getCount()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieViewCell", for: indexPath) as! MovieCollectionViewCell
        if let moviesModel = MessageManager.sharedInstance.getMessage(withIndex: collectionView.tag) as? Movies,
            let movie = moviesModel.getMovie(withIndex: indexPath.row){
            cell.setupCell(withMessage: movie)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let moviesModel = MessageManager.sharedInstance.getMessage(withIndex: collectionView.tag) as? Movies,
            let movie = moviesModel.getMovie(withIndex: indexPath.row), let imdbID = movie.imdbID {
            let link = Movie.imdbStaticLink + imdbID
            openLinkViewController(withLink: link)
        }

    }
    
    func openLinkViewController(withLink link: String) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Link") as? LinkViewController {
            controller.link = link
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    //MARK: Chat Action Sheet
    
    @IBAction func startChatClicked(_ sender: Any){
        let actionController = CustomActionController()
        actionController.headerData = HeaderData(title: "Get recommendations...", image: UIImage(named: "chat_icon")!)
        actionController.addAction(Action((ActionData(title: "From ratings")), style: .default, handler: { action in
            MessageManager.sharedInstance.removeMessagesFromQueue()
            self.hideButtons()
            var link = "rate-start"
            if UserDefaults.standard.value(forKey: "userEmotion") == nil  {
                link = "user-opens-app-2"
            }
            self.openScenario(link: link, title: "Let's rate")}))
        actionController.addAction(Action((ActionData(title: "Specific")), style: .default, handler: { action in
            MessageManager.sharedInstance.removeMessagesFromQueue()
            self.hideButtons()
            self.openScenario(link: "questions-start", title: "Let's talk")
        }))
        self.present(actionController, animated: true, completion: nil)
    }
    
    @IBAction func openWishlist(_ sender: Any) {
        self.hideButtons()
        self.openScenario(link: "wishlist-start", title: "Wishlist")
    }
    
    func setupOptions(choices: [[String : Any]]){
        if StateManager.sharedInstance.configuration == .keyboard {
            setupKeyboard(withArray: choices)
        }
        else if StateManager.sharedInstance.configuration == .buttons {
            refreshAllButtons(withArray: choices)
        }
    }
    
    func openScenario(link: String, title: String){
        StateManager.sharedInstance.redirectScenario(link: link, title: title)
    }
    
    func showOptions(){
        if let choices = StateManager.sharedInstance.currentChoices {
            setupOptions(choices: choices)
        }
    }

}
