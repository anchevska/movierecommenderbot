//
//  LinkViewController.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 6/3/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LinkViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var shadowView: UIView!
    var link : String?
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
        shadowView.layer.shadowRadius = 1
        
        if let link = link {
            webView.loadRequest(URLRequest(url: URL(string: link)!))
        }
    
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50), type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor(colorLiteralRed: 88/255, green: 64/255, blue: 173/255, alpha: 1.0))
        view.addSubview(activityIndicator)
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: WebView Delegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.stopAnimating()
    }

}
