//
//  MessageManager.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 3/27/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import Alamofire

class MessageManager: NSObject {
    
    static let sharedInstance = MessageManager()
    
    var timer : Timer!
    var refreshType : RefreshType = .insert
    var messages : [BaseMessage]
    var queue : [BaseMessage]
    
    override init() {
        messages = [BaseMessage]()
        queue = [BaseMessage]()
        super.init()
//        UserManager.sharedInstance.uid = "L0lPCryl25R5S7lNutIcfFSRfiB2"
//        UserManager.sharedInstance.databaseID = "58f3cbd8de914e42565d6c9e"
        getInitialMessages()
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: Constants.delay, target: self, selector: #selector(onTick), userInfo: nil, repeats: !queue.isEmpty)
    }
    
    
    func onTick(){
        addMessageToTable()
    }
    
    func getInitialMessages(){
        Alamofire.request("http://127.0.0.1:5000/getInitialMessages", method: .post, parameters: ["id":UserManager.sharedInstance.databaseID], encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            if let resp = response.response {
                if let responseArray = response.result.value as? [[String:Any]], resp.statusCode == 201 {
                    for conversationEntry in responseArray {
                        if let source = conversationEntry["source"] as? Int {
                            self.messages.append(MessageFactory.createMessage(withDict: conversationEntry, fromSource: source))
                            self.notifyObservers()
                        }
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "initialLoadingFinished"), object: nil)
                }
            }
        }
    }
    
    func notifyObservers(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modelRefreshed"), object: nil)
    }
    
    func getMessage(withIndex index: Int) -> BaseMessage? {
        return index < messages.count ? messages[index] : nil
    }
    
    func addMessages(withChoiceTitle title: String, andMessages msgs: [[String:Any]]) {
        if title != "" {
            messages.append(Message(message: title, source: MessageSource.user.rawValue))
        }
        for message in msgs {
            if let src = message["source"] as? Int {
                queue.append(MessageFactory.createMessage(withDict: message, fromSource: src))
            }
        }
        refreshType = .insert
        notifyObservers()
        perform(#selector(addLoadingIndicator), with: nil, afterDelay: 0.5)
        startTimer()
    }
    
    func addMessages(messages: [BaseMessage]) {
        queue.append(contentsOf: messages)
    }
    
    func replace(message msg: BaseMessage, fromIndex index: Int) {
        messages[index] = msg
    }
    
    func addMessageToTable() {
        if !queue.isEmpty {
            messages[messages.count - 1] = queue.removeFirst()
            refreshType = .reload
            notifyObservers()
            if queue.isEmpty {
                timer.invalidate()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "messageSent"), object: nil)
            }
            else {
                addLoadingIndicator()
            }
        }
    }
    
    func addLoadingIndicator(){
        messages.append(Message(message: "hel", source: MessageSource.loading.rawValue))
        refreshType = .insert
        notifyObservers()
    }
    
    func removeMessagesFromQueue(){
        if let timer = timer {
            timer.invalidate()
        }
        if let lastSource = messages.last?.source, lastSource == MessageSource.loading, !queue.isEmpty {
            messages[messages.count - 1] = queue.removeFirst()
            queue = [BaseMessage]()
            refreshType = .reload
            notifyObservers()
        }
        else {
            queue = [BaseMessage]()
        }
    }
}
