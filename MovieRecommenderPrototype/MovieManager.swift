//
//  MovieManager.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 4/16/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import Alamofire

class MovieManager: NSObject {
    
    var movies: [Movie]!
    static let sharedInstance = MovieManager()
    
    func setMoviesFromDict(dict: [String: Any]){
        movies = [Movie]()
        for key in dict.keys {
            movies.append(Movie(dict: dict[key] as! [String: Any]))
        }
        MessageManager.sharedInstance.addMessages(messages: [movies.first!])
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modelRefreshed"), object: nil)
    }
    
    func ratingAdded(movie: Movie, rating: CGFloat){
        if let id = movie.movieID, let type = StateManager.sharedInstance.type, let emotion = UserDefaults.standard.value(forKey: "userEmotion") as? String {
            var emotion_id = ""
            if emotion == StateType.happyUser.rawValue {
                emotion_id = UserManager.sharedInstance.happyID
            }
            else if emotion == StateType.sadUser.rawValue {
                emotion_id = UserManager.sharedInstance.sadID
            }
            else {
                emotion_id = UserManager.sharedInstance.databaseID
            }
            let params = ["uid": UserManager.sharedInstance.uid,
                          "movieID": id,
                          "rating": rating,
                          "type" : type.rawValue,
                          "user_id":emotion_id] as [String : Any]
            Alamofire.request("http://127.0.0.1:5000/addRating", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                if let resp = response.response {
                    if resp.statusCode == 201 {
                        print("added rating")
                        if let movieDict = response.result.value as? [String:Any] {
                            self.replaceMovie(dict: movieDict)
                        }
                    }
                }
            }
        }
    }
    
    func notInterested(movie:Movie){
        if let id = movie.movieID, let type = StateManager.sharedInstance.type {
            let params = [ "uid": UserManager.sharedInstance.uid,
                           "movieID": id,
                           "type": type.rawValue,
                           "user_id":UserManager.sharedInstance.databaseID] as [String : Any]
            Alamofire.request("http://127.0.0.1:5000/notInterested", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                if let resp = response.response {
                    if resp.statusCode == 201 {
                        print("not interested")
                        if let movieDict = response.result.value as? [String:Any] {
                            self.replaceMovie(dict: movieDict)
                        }
                    }
                }
            }
        }
    }
    
    func addToWishlist(movie:Movie){
        if let id = movie.movieID, let type = StateManager.sharedInstance.type {
            let params = [ "uid": UserManager.sharedInstance.uid,
                           "movieID": id,
                           "type": type.rawValue,
                           "user_id":UserManager.sharedInstance.databaseID] as [String : Any]
            Alamofire.request("http://127.0.0.1:5000/addToWishlist", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                if let resp = response.response {
                    if resp.statusCode == 201 {
                        print("added to wishlist")
                        if let movieDict = response.result.value as? [String:Any] {
                            self.replaceMovie(dict: movieDict)
                        }
                    }
                }
            }
        }
    }
    
    func replaceMovie(dict: [String: Any]){
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            MessageManager.sharedInstance.replace(message: Movies(dict: dict), fromIndex: MessageManager.sharedInstance.messages.count - 1)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modelRefreshed"), object: nil)
        }
    }
}
