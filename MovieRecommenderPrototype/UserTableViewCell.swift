//
//  UserTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 4/1/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class UserTableViewCell: MessageTableViewCell {

    @IBOutlet var bubbleWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupCell(withMessage message: BaseMessage) {
        let message = message as! Message
        super.setupCell(withMessage: message)
        let heightPadding : CGFloat = 24
        let widthPadding : CGFloat = 16
        let roundedView = UIView(frame: CGRect(x:16,y:0,width:message.size.width + widthPadding ,height:message.size.height + heightPadding))
        let maskPath = UIBezierPath(roundedRect: roundedView.bounds,
                                    byRoundingCorners: [.topRight, .bottomLeft, .topLeft],
                                    cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        view.layer.mask = shape
        
        labelWidthConstraint.constant = message.size.width + 8
        bubbleWidthConstraint.constant = message.size.width + widthPadding
        layoutIfNeeded()
    }

}
