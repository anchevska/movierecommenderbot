//
//  LoginViewController.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 3/1/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn
import FBSDKLoginKit
import Firebase
import FirebaseAuth
import Alamofire

class LoginViewController: UIViewController, GIDSignInUIDelegate,GIDSignInDelegate {
    
    private var _fbLoginManager: FBSDKLoginManager?
    
    var fbLoginManager: FBSDKLoginManager {
        get {
            if _fbLoginManager == nil {
                _fbLoginManager = FBSDKLoginManager()
            }
            return _fbLoginManager!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        do {
//            try FIRAuth.auth()?.signOut()
//        }
//        catch let signOutError as NSError {
//            print ("Error signing out: \(signOutError)")
//        }
        if let user = FIRAuth.auth()?.currentUser {
            UserManager.sharedInstance.uid = user.uid
            self.addUser(uid: user.uid)
        }
    }
    
    //MARK: Sign in with facebook
    
    @IBAction func signInWithFacebook(_ sender: Any) {
        let permissions = ["public_profile", "email"]
        fbLoginManager.logIn(withReadPermissions: permissions, from: self) { (result, error) in
            if let err = error {
                print("error login with facebook \(err.localizedDescription)")
            }else if let res = result, res.isCancelled {
                print("user cancelled facebook login")
            }
            else {
                let credentials = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.signInWithCredentials(credentials)
            }
        }
    }
    
    //MARK: Sign in with google
    
    @IBAction func signInWithGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print("error login with google \(error.localizedDescription)")
        }
        else {
            guard let authentication = user.authentication else { return }
            let credentials = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                               accessToken: authentication.accessToken)
            signInWithCredentials(credentials)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signInAnonymous(_ sender: Any) {
        FIRAuth.auth()?.signInAnonymously() { (user, error) in
            if let _ = error {
                print("error sign in anonymous with firebase")
            }
            else {
                if let user = user {
                    UserManager.sharedInstance.uid = user.uid
                    self.addUser(uid: user.uid)
                }
            }
        }
    }
    
    func signInWithCredentials(_ credentials: FIRAuthCredential){
        FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
            if let _ = error {
                print("error sign in with firebase")
            }
            else {
                if let user = user {
                    UserManager.sharedInstance.uid = user.uid
                    self.addUser(uid: user.uid)
                }
            }
        })
    }
    
    func openMovieViewController(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as? ChatViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func addUser(uid: String){
        Alamofire.request("http://127.0.0.1:5000/getUser", method: .post, parameters: ["uid":uid], encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            if let resp = response.response {
                if let responseDict = response.result.value as? [String: String], resp.statusCode == 201 {
                    UserManager.sharedInstance.databaseID = responseDict["_id"]
                    UserManager.sharedInstance.happyID = responseDict["happy_id"]
                    UserManager.sharedInstance.sadID = responseDict["sad_id"]
                    self.openMovieViewController()
                }
            }
        }
    }
}
