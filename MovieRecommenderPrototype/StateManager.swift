//
//  StateManager.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 4/16/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit
import Alamofire

class StateManager: NSObject {
    
    static let sharedInstance = StateManager()
    var type: StateType?
    var configuration : OptionConfiguration?
    var currentChoices : [[String : Any]]?
    var genre : String?
    var year : String?
    
    func getChoices(callback:@escaping (([[String: Any]]) -> Void)){
        Alamofire.request("http://127.0.0.1:5000/getChoices", method: .post, parameters: ["id":UserManager.sharedInstance.databaseID], encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            if let resp = response.response {
                if let responseDict = response.result.value as? [String: Any], resp.statusCode == 201,
                let choices = responseDict["choices"] as? [[String:Any]],
                let config = responseDict["configuration"] as? Int {
                    if let option = OptionConfiguration(rawValue: config){
                        self.configuration = option
                    }
                    callback(choices)
                }
            }
            else {
                callback([])
            }
        }
    }
    
    func redirectScenario(link: String, title: String) {
        var emotion = ""
        if let userEmotion = UserDefaults.standard.value(forKey: "userEmotion") as? String {
            emotion = userEmotion
        }
        var emotion_id = ""
        if emotion == StateType.happyUser.rawValue {
            emotion_id = UserManager.sharedInstance.happyID
        }
        else if emotion == StateType.sadUser.rawValue {
            emotion_id = UserManager.sharedInstance.sadID
        }
        else {
            emotion_id = UserManager.sharedInstance.databaseID
        }
        var params :[String : Any] = ["id":UserManager.sharedInstance.databaseID, "link":link, "title":title, "recommendation_id":emotion_id]
        if let year = year,  let genre = genre {
            params["year"] = year
            params["genre"] = genre
        }
        Alamofire.request("http://127.0.0.1:5000/redirectScenario", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            if let resp = response.response {
                if let responseArray = response.result.value as? [String: Any], resp.statusCode == 201 {
                    if let type = responseArray["type"] as? String {
                        self.type = StateType(rawValue: type)
                        if self.type == .sadUser || self.type == .happyUser {
                            UserDefaults.standard.setValue(self.type?.rawValue, forKey: "userEmotion")
                        }
                    }
                    if let choices = responseArray["choices"] as? [[String:Any]],
                        let config = responseArray["configuration"] as? Int,
                        let messages = responseArray["messages"] as? [[String:Any]]{
                        if let option = OptionConfiguration(rawValue: config){
                            self.configuration = option
                        }
                        self.currentChoices = choices
                        MessageManager.sharedInstance.addMessages(withChoiceTitle: title, andMessages: messages)
                    }
                }
            }
        }
        
    }
}
