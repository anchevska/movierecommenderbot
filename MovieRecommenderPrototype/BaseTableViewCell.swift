//
//  BaseTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Ancevska on 4/16/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    @IBOutlet var messageLabel : UILabel!

    func setupCell(withMessage message: BaseMessage){
        messageLabel.text = message.message
    }

}
