//
//  BotTableViewCell.swift
//  MovieRecommenderPrototype
//
//  Created by Viktorija Anchevska on 3/27/17.
//  Copyright © 2017 Viktorija Anchevska. All rights reserved.
//

import UIKit

class BotTableViewCell: MessageTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setupCell(withMessage message: BaseMessage) {
        let message = message as! Message
        super.setupCell(withMessage: message)
        let padding : CGFloat = 24
        let roundedView = UIView(frame: CGRect(x:16,y:0,width:message.size.width + padding,height:message.size.height + padding))
        let maskPath = UIBezierPath(roundedRect: roundedView.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight, .topLeft],
                                    cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        view.layer.mask = shape
        
        labelWidthConstraint.constant = message.size.width + 8
        
        layoutIfNeeded()
    }

}
